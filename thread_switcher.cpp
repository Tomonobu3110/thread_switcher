#include <iostream>
#include <thread>
#include <mutex>

#include <stdio.h>
#include <unistd.h>

///////////////////////////////////////
// Thread Switcher
//
class ThreadSwitcher {
public:
    ThreadSwitcher() : context_(0) {} 

    void wait_until(int ctx) {
        printf("ctx %d : enter wait_until\n", ctx);
        while (wait(ctx)) { usleep(1000); }// 1msec (TBD) 
        printf("ctx %d : exit wait_until\n", ctx);
    }
    
    bool wait(int ctx) {
        printf("ctx %d : context %d\n", ctx, context_);
        mtx_.lock();
        if (context_ != ctx) {
            mtx_.unlock();
            return true;
        }
        return false;
    }
    
    void switch_to(int ctx) {
        context_ = ctx;
        mtx_.unlock();
    }
private:
    int context_;
    std::mutex mtx_;
};

///////////////////////////////////////
// test (main)
//
int main()
{
    ThreadSwitcher ts;
    
    auto func = [](int ctx, ThreadSwitcher* pts) {
        while (1) {
            pts->wait_until(ctx);
            printf("enter %d\n", ctx);
            sleep(1);
            printf("exit %d\n", ctx);
            pts->switch_to(1 - ctx);
        }
    };
    
    std::thread t1(func, 0, &ts);
    std::thread t2(func, 1, &ts);
    
    t1.join();
    t2.join();
}